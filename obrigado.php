<?php
	$titulo = "Obrigado pelo Contato | CS SAT Soluções em TVs";
	$descricao = "Obrigado pelo contato";
	$page = "index";
	$canonical = "https://www.cssat.com.br/thank-you.php";	

	require_once('includes/header.php');
?>

<section class="thank-you">
	<div class="container">
		<div class="row">
			<h1 class="title-1 text-center">Agradecemos a <span>sua mensagem</span></h1>
			<p class="text-center">Em breve retornaremos o contato.</p>
			<a href="index.php" class="bt-2 text-center">Voltar para a Home</a>
		</div>
	</div>
</section>

<?php require_once('includes/footer-thank-you.php') ?>	

