$(document).ready(function(){

	// DEPOIMENTOS
	$('.owl-carousel.grupo').owlCarousel({
	    loop:true,
	    margin:30,
	    nav:true,
	    responsive:{
	        0:{
	            items:1
	        },
	        769:{
	            items:3
	        },
	        1200:{
	            items:4
	        }
	    }
	});

	// NOSSOS PLANOS
	$(".plano-2").css("display","none");
	$(".plano-3").css("display","none");
	$(".plano-4").css("display","none");

	$('.owl-carousel.plano').owlCarousel({
	    loop:false,
	    margin:30,
	    nav:false,
	    mouseDrag:false,
	    responsive:{
	        0:{
	            items:1,
	            mouseDrag:true
	        },
	        769:{
	            items:3
	        }
	    }
	});	

	function changeContent(n, target){
		$(".plano").hide();
		$(".plano-"+n).fadeIn();
		$(".bt").removeClass("active")
		$(target).addClass("active");
	}

	$(".mensal").click(function(){
		changeContent(1, this);
	});

	$(".bimestral").click(function(){
		changeContent(2, this);
	});

	$(".trimestral").click(function(){
		changeContent(3, this);
	});

	$(".semestral").click(function(){
		changeContent(4, this);
	});	

	// NOSSOS DIFERENCIAIS
	$('.owl-carousel.diferencial').owlCarousel({
	    loop:false,
	    margin:30,
	    nav:false,
	    mouseDrag:false,
	    responsive:{
	        0:{
	            items:1,
	            mouseDrag:true
	        },
	        769:{
	            items:3
	        }
	    }
	});

	// BLOG
	$('.owl-carousel.artigos').owlCarousel({
	    loop:false,
	    margin:0,
	    nav:false,
	    mouseDrag:false,
	    responsive:{
	        0:{
	            items:1,
	            mouseDrag:true
	        },
	        769:{
	            items:2
	        },
	        1200:{
	            items:3
	        }
	    }
	});

	// PÁGINA INTERNA =- RECEPTORES
	$('.owl-carousel.lista-receptores').owlCarousel({
	    loop:false,
	    margin:0,
	    nav:false,
	    mouseDrag:false,
	    responsive:{
	        0:{
	            items:1,
	            mouseDrag:true
	        },
	        769:{
	            items:3
	        },
	        1200:{
	            items:6
	        }
	    }
	});	


	// COMPRAR OU RENOVAR
	function changeContent2(n, target){
		$(".comprar-ou-renovar .item").hide();
		$(".comprar-ou-renovar .item-"+n).fadeIn();
		$(".comprar-ou-renovar .bt").removeClass("active")
		$(target).addClass("active");
	}

	$(".pague").click(function(){
		changeContent2(1, this);
		$(".bar").css("left","17%");
	});

	$(".comprove").click(function(){
		changeContent2(2, this);
		$(".bar").css("left","50%");
	});	

	$(".acesse").click(function(){
		changeContent2(3, this);
		$(".bar").css("left","83%");
	});	

	// WHATSAPP
	$(".close-whats").click(function(){
		$(".whatsapp").addClass('close-whatsapp');
	});


	// SCROLL
	$("a[href^='#']").click(function(e) {
		e.preventDefault();
		
		var position = $($(this).attr("href")).offset().top;

		$("body, html").animate({
			scrollTop: position - 2000
		} /* speed */ );
	});	


});









//- FUNCTION FOR SCROLL EVENTS 
//-------------------------------------------

if (document.querySelector('.trigger') != undefined) {
	window.onscroll = getScrollPosition;
}


function getScrollPosition(){
	var windowBottom = window.pageYOffset + window.innerHeight,
	object = document.querySelector('.trigger'), 
	objectBottom = object.offsetTop + parseInt(window.getComputedStyle(object).height);

	if (windowBottom > objectBottom - 200){
		object.classList.add('go');
	}
};

//- AJAX PROMISE TOOLS
//-------------------------------------------------

var tools = {
	siteUrl : '', 
	url : 'url',

	postRequest : function(url, data) { 
		var x = new XMLHttpRequest();
		return new Promise(function(resolve, reject){	
			x.open('POST', url, data); 
			x.onload = function() {
				if (this.status > 199 && this.status < 300) {
					resolve(this.response)
				}
				else {
					reject({
						status: this.status, 
						statusText: x.statusText
					}); 
				}
			}; 
			x.onError = function() {
				reject({
					status : this.status, 
					statusText : x.statusText 
				}); 
			}; 
			x.send(url, data);
		})
	}, 

	getRequest : function(url) {
		this.x.onreadystatechange = function(response){};
		this.x.open('GET', url || null); 
		this.x.send(null);
		return this.x;  
	}
}; 


//- IE FIX
//-------------------------------------------------

(function msiedetection() {
	var ie = window.navigator.userAgent.indexOf("MSIE ")
	// If Internet Explorer, return true
	if (ie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  {
		document.querySelector('html').classList.add('ie');
	}
	// If another browser, return false
	else  {
		console.log('thank you for not using ie')
		return false;
	}	
})();


//- FIREFOX FIX 
//-------------------------------------------------

if (navigator.userAgent.toLowerCase().indexOf('firefox') > -1){
	document.querySelector('html').classList.add('firefox')
}
