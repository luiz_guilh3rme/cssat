<?php
	$titulo = "Teste CS SKY | CS SAT Soluções em TVs";
	$descricao = "Ao assinar o plano Teste CS SKY, o usuário tem acesso a mais de 500 canais em Full HD, próprios da operadora NET. Ficou com dúvida? Acesse ja e entenda mais!";
	$page = "internas";
	$canonical = "https://www.cssat.com.br/cs-sky.php";	

	require_once('includes/header.php');
?>

<section class="descritivo">
	<div class="container">
		<div class="col-md-12">
			<div class="col-lg-7 col-md-12">				
				<h1 class="title-1 text-left">
					CS <span>SKY</span>
				</h1>
			</div>
			<div class="col-lg-12 col-md-12">				
				<picture>
				  <source media="(max-width: 768px)" srcset="images/cs-sky-mobile.png">
				  <img src="images/webp/cs-sky.webp" width="546" height="582" alt="Teste CS SKY" title="Teste CS SKY">
				</picture>	
			</div>
			<div class="col-lg-7 col-md-12">	
				<p>O Teste CS SKY disponibiliza a programação completa da operadora, além de suporte sempre que precisar ou tiver dúvidas por meio do chat disponível no site.</p>						
				<p>A Sky é uma empresa pioneira em trazer serviços mais interativos no Brasil, fazendo com que o plano Teste CS SKY seja uma excelente vantagem, já que você poderá usufruir de seus benefícios de forma muito mais econômica e fácil.</p>
				<p>Essa tecnologia consiste em compartilhar o sinal receptor das operadoras de TV à cabo entre outros usuários, também decodificando o acesso para todos os outros canais, fazendo com que o cliente tenha acesso a todos os canais e filme da operadora Sky.</p>
				<p>Adquirindo um dos planos (mensal, bimestral, trimestral ou semestral), você pode assistir os conteúdos da operadora Sky sem travamentos e interrupções e em altíssima qualidade HD.</p>
				<p>Conheça os nossos planos e faça o teste agora mesmo!</p>
			</div>
		</div>

	</div>
</section>

<section class="nossos-planos internas">
	<div class="container">

		<div class="col-md-8">

			<h2 class="title-1 color-2">
				Conheça <span>Nossos Planos</span>
			</h2>
			<p class="color-2">Veja as opções para as operadoras Claro, Sky e Net, em planos mensal, bimestral, trimestral ou semestral. O pagamento é feito de forma rápida e fácil, com um valor que cabe no seu bolso, além de exibição de mais de 500 canais em altíssima qualidade. Comprove fazendo o teste gratuitamente!</p>

			<div class="list">
				<p>
					<i class="bt mensal sprite sprite-radio-disable active"></i>Mensal
				</p>
				<p>
					<i class="bt bimestral sprite sprite-radio-disable"></i>Trimestral
				</p>
				<p class="trimestral-label">
					<i class="bt trimestral sprite sprite-radio-disable"></i>Semestral
				</p>
				<p>
					<i class="bt semestral sprite sprite-radio-disable"></i>Anual
				</p>
			</div>	
			
			<a class="bt-3 hidden-xs teste-sky" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!"><i class="fas fa-play"></i>Quero Testar Grátis!</a>

		</div>

		<div class="col-md-4">
			<ul class="plano plano-1">
				<li class="item text-center">
					<img src="images/logo-sky.png" width="153" height="137" alt="SKY" title="SKY">
					<h3 class="title-plan">Plano SKY</h3>
					<p class="valor"><span>R$</span>15.00</p>
					<ul>
						<li>Operadora SKY TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul>					
				</li>
			</ul>

			<ul class="plano plano-2">
				<li class="item text-center">
					<img src="images/logo-sky.png" width="153" height="137" alt="SKY" title="SKY">
					<h3 class="title-plan">Plano SKY</h3>
					<p class="valor"><span>R$</span>45.00</p>
					<ul>
						<li>Operadora SKY TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul>					
				</li>
			</ul>

			<ul class="plano plano-3">
				<li class="item text-center">
					<img src="images/logo-sky.png" width="153" height="137" alt="SKY" title="SKY">
					<h3 class="title-plan">Plano SKY</h3>
					<p class="valor"><span>R$</span>80.00</p>
					<ul>
						<li>Operadora SKY TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul					
				</li>
			</ul>

			<ul class="plano plano-4">
				<li class="item text-center">
					<img src="images/logo-sky.png" width="153" height="137" alt="SKY" title="SKY">
					<h3 class="title-plan">Plano SKY</h3>
					<p class="valor"><span>R$</span>155.00</p>
					<ul>
						<li>Operadora SKY TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul>					
				</li>
			</ul>
		</div>

		<div class="col-xs-12 hidden-lg hidden-md hidden-sm">
			<a class="bt-3" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!"><i class="fas fa-play"></i>Quero Testar Grátis!</a>
		</div>

	</div>
</section>

<section class="receptores">
	<div class="container">

		<div class="col-md-12">
			<h2 class="title-1 text-center">Receptadores<span>Suportados</span></h2>
			<p class="text-center">Você terá acesso a canais de tv fechado e aberto para os receptores abaixo.</p>
		</div>

		<ul class="owl-carousel owl-theme lista-receptores">
			<li class="item">
				<img src="images/receptores/r1.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r2.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r3.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r4.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r5.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r6.jpg" width="167" height="46" alt="" title="">
			</li>	
			<li class="item">
				<img src="images/receptores/r7.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r8.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r9.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r10.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r11.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r12.jpg" width="167" height="46" alt="" title="">
			</li>	
			<li class="item">
				<img src="images/receptores/r13.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r14.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r15.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r16.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r17.jpg" width="167" height="46" alt="" title="">
			</li>		
			<li class="item">
				<img src="images/receptores/r18.jpg" width="167" height="46" alt="" title="">
			</li>																						
		</ul>

		<p class="quero-ver-mais">
			<i class="fas fa-angle-double-left"></i>
				Deslize para ver mais.
			<i class="fas fa-angle-double-right"></i>
		</p>

	</div>
</section>

<?php require_once('includes/footer.php') ?>	

