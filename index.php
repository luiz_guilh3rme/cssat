<?php
	$titulo = "Teste CS | CS SAT Soluções em TVs";
	$descricao = "A CS SAT hoje é considerada a melhor empresa de cardsharing do Brasil, com central de atendimento fixa. Clique aqui e conheça mais sobre nós!";
	$page = "index";
	$canonical = "https://www.cssat.com.br/index.php";
	require_once('includes/header.php');

?>

	<section class="teste-gratis">
		<div class="container">
			<div class="col-md-5 col-sm-12">
				<h1 class="title-1 text-left">
					Solicite um <span>Teste CS Grátis</span>
				</h1>
				<p>Confira a qualidade de nossos serviços de CardSharing, com o teste CS. Para solicitar, basta ter internet e uma antena compatível com outros receptores. <b>O teste CS é grátis e válido por 24 horas.</b></p>
				<a href="teste.php" rel="nofollow noopener" class="bt-1 teste-body" target="_blank" title="Faça um teste agora!"><i class="fas fa-play"></i>Testar</a>
				<div class="group">						
					<a href="cs-claro.php" title="Ir para CS Claro">
						<img src="images/logo-claro.png" width="80" height="72" alt="Claro" title="Ir para CS Claro">
					</a>
					<a href="cs-sky.php" title="Ir para CS SKY">
						<img src="images/logo-sky.png" width="80" height="72" alt="SKY" title="Ir para CS SKY">
					</a>
					<a href="cs-net.php" title="Ir para CS NET">
						<img src="images/logo-net.png" width="80" height="72" alt="NET" title="Ir para CS NET">
					</a>
				</div>
			</div>
		</div>
	</section>

	<section class="nossos-planos">
		<div class="container">

			<div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0">
				<h2 class="title-1 text-center">
					Conheça <span>Nossos Planos</span>
				</h2>
				<p class="text-center">Temos planos para as operadoras Claro, Sky e Net, com vantagens para o plano mensal, bimestral, trimestral ou semestral. <b>Faça o teste de cs grátis e descubra a melhor opção para você!</b></p>
			</div>

			<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">

				<div class="list">
					<p>
						<i class="bt mensal sprite sprite-radio-disable active"></i>Mensal
					</p>
					<p>
						<i class="bt bimestral sprite sprite-radio-disable"></i>Trimestral
					</p>
					<p class="trimestral-label">
						<i class="bt trimestral sprite sprite-radio-disable"></i>Semestral
					</p>
					<p>
						<i class="bt semestral sprite sprite-radio-disable"></i>Anual
					</p>
				</div>

				<ul class="owl-carousel owl-theme plano plano-1">
					<li class="item text-center">
						<a href="cs-claro.php" title="Ir para CS Claro" >
							<img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
						</a>
						<h3 class="title-plan">Teste CS Claro</h3>
						<p class="valor"><span>R$</span>15.00</p>
						<ul>
							<li>Operadora Claro TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-claro-M teste-claro" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>
					<li class="item text-center">
						<a href="cs-sky.php" title="Ir para CS SKY">
							<img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
						</a>
						<h3 class="title-plan">Teste CS Sky</h3>
						<p class="valor"><span>R$</span>15.00</p>
						<ul>
							<li>Operadora SKY TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-sky-M teste-sky" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>							
					<li class="item text-center">
						<a href="cs-net.php" title="Ir para CS NET">
							<img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
						</a>
						<h3 class="title-plan">Teste CS Net</h3>
						<p class="valor"><span>R$</span>20.00</p>
						<ul>
							<li>Operadora NET TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-net-M teste-net" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>
				</ul>

				<ul class="owl-carousel owl-theme plano plano-2">
					<li class="item text-center">
						<a href="cs-claro.php" title="Ir para CS Claro">
							<img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
						</a>
						<h3 class="title-plan">Teste CS Claro</h3>
						<p class="valor"><span>R$</span>45.00</p>
						<ul>
							<li>Operadora Claro TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-claro-T teste-claro" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>
					<li class="item text-center">
						<a href="cs-sky.php" title="Ir para CS SKY">
							<img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
						</a>
						<h3 class="title-plan">Teste CS Sky</h3>
						<p class="valor"><span>R$</span>45.00</p>
						<ul>
							<li>Operadora SKY TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-sky-T teste-sky" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>							
					<li class="item text-center">
						<a href="cs-net.php" title="Ir para CS NET">
							<img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
						</a>
						<h3 class="title-plan">Teste CS Net</h3>
						<p class="valor"><span>R$</span>60.00</p>
						<ul>
							<li>Operadora NET TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-net-T teste-net" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>
				</ul>

				<ul class="owl-carousel owl-theme plano plano-3">
					<li class="item text-center">
						<a href="cs-claro.php" title="Ir para CS Claro">
							<img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
						</a>
						<h3 class="title-plan">Teste CS Claro</h3>
						<p class="valor"><span>R$</span>80.00</p>
						<ul>
							<li>Operadora Claro TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-claro-S teste-claro" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>
					<li class="item text-center">
						<a href="cs-sky.php" title="Ir para CS SKY">
							<img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
						</a>
						<h3 class="title-plan">Teste CS Sky</h3>
						<p class="valor"><span>R$</span>80.00</p>
						<ul>
							<li>Operadora SKY TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-sky-S teste-sky" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
					</li>							
					<li class="item text-center">
						<a href="cs-net.php" title="Ir para CS NET">
							<img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
						</a>
						<h3 class="title-plan">Teste CS Net</h3>
						<p class="valor"><span>R$</span>110.00</p>
						<ul>
							<li>Operadora NET TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-net-S teste-net" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!" >Teste aqui</a>
					</li>
				</ul>

				<ul class="owl-carousel owl-theme plano plano-4">
					<li class="item text-center">
						<a href="cs-claro.php" title="Ir para CS Claro">
							<img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
						</a>
						<h3 class="title-plan">Teste CS Claro</h3>
						<p class="valor"><span>R$</span>155.00</p>
						<ul>
							<li>Operadora Claro TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-claro-A teste-claro" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!" >Teste aqui</a>
					</li>
					<li class="item text-center">
						<a href="cs-sky.php" title="Ir para CS SKY">
							<img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
						</a>
						<h3 class="title-plan">Teste CS Sky</h3>
						<p class="valor"><span>R$</span>155.00</p>
						<ul>
							<li>Operadora SKY TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-sky-A teste-sky" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!" >Teste aqui</a>
					</li>							
					<li class="item text-center">
						<a href="cs-net.php" title="Ir para CS NET">
							<img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
						</a>
						<h3 class="title-plan">Teste CS Net</h3>
						<p class="valor"><span>R$</span>210.00</p>
						<ul>
							<li>Operadora NET TV</li>
							<li>Grade Completa de Canais</li>
							<li>Mais de 500 Canais</li>
							<li>Servidor Dedicado e Exclusivo</li>
							<li>Suporte 7 dias por semana</li>
							<li>Teste 24 Horas</li>
						</ul>
						<a class="bt-2 teste-net-A teste-net" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!" >Teste aqui</a>
					</li>
				</ul>	

				<p class="quero-ver-mais">
					<i class="fas fa-angle-double-left"></i>
						Deslize para ver mais.
					<i class="fas fa-angle-double-right"></i>
				</p>

				<a class="bt-3 teste-planos" href="teste.php" rel="nofollow noopener" target="_blank" title="Faça um teste agora!"><i class="fas fa-play"></i>Quero Testar Grátis!</a>
			</div>

		</div>
	</section>

	<section class="comprar-ou-renovar">
		<div class="container">
			<h2 class="title-1 color-2 text-center">
				PASSO A PASSO<span>COMPRAR OU RENOVAR</span>
			</h2>
			<p class="principal color-2 text-center">Quer comprar ou renovar o seu plano? Confira o passo a passo abaixo e veja como é simples adquirir o seu login!</p>				

			<div class="img-char col-lg-7 col-md-8 col-lg-offset-0 col-md-offset-2">
				<img src="images/personagens.png" alt="Compre ou renove seu plano CS SAT" title="Comprar ou renovar plano CS SAT">
			</div>				

			<div class="col-lg-5 col-md-12">

				<div class="list">
					<p class="bt pague sprite sprite-ico-pague active">Pague</p>
					<p class="bt comprove sprite sprite-ico-comprove">Comprove</p>
					<p class="bt acesse sprite sprite-ico-acesse">Acesse</p>
					<span class="bar"></span>
				</div>

				<div class="item item-1">
					<p>1- Após efetuar pagamento, envie comprovante via chat, whatsapp ou email.</p>
					<p class="detail">Passo a passo para envio do comprovante:</p>
					<ul>
						<li>• Aceitamos somente arquivos com os seguintes formatos:  .jpg, .png, .pdf, .doc ou .html;</li>
						<li>• Aceitamos somente comprovantes com no máximo 5 dias após pagamento, clientes que enviarem comprovantes após este período;</li>não terão seu login liberado;
						<li>• O arquivo enviado deve ter um tamanho máximo de 3MB;</li>
						<li>• Não serão aceitos comprovantes ilegíveis;</li>
						<li>• Em caso de pagamentos por depósito ou transferência é OBRIGATÓRIO comprovante de compra como FOTO ou IMAGEM;</li>
						<li>• O prazo de liberação após envio do comprovante pelo nosso site é de até 24hrs. Favor sempre aguardar este prazo.</li>				
					</ul>
				</div>

				<div class="item item-2">
					<p>2- Após efetuar pagamento, envie comprovante via chat, whatsapp ou email.</p>
					<p class="detail">Passo a passo para envio do comprovante:</p>
					<ul>
						<li>• Aceitamos somente arquivos com os seguintes formatos:  .jpg, .png, .pdf, .doc ou .html;</li>
						<li>• Aceitamos somente comprovantes com no máximo 5 dias após pagamento, clientes que enviarem comprovantes após este período;</li>não terão seu login liberado;
						<li>• O arquivo enviado deve ter um tamanho máximo de 3MB;</li>
						<li>• Não serão aceitos comprovantes ilegíveis;</li>
						<li>• Em caso de pagamentos por depósito ou transferência é OBRIGATÓRIO comprovante de compra como FOTO ou IMAGEM;</li>
						<li>• O prazo de liberação após envio do comprovante pelo nosso site é de até 24hrs. Favor sempre aguardar este prazo.</li>				
					</ul>
				</div>

				<div class="item item-3">
					<p>3 - Após efetuar pagamento, envie comprovante via chat, whatsapp ou email.</p>
					<p class="detail">Passo a passo para envio do comprovante:</p>
					<ul>
						<li>• Aceitamos somente arquivos com os seguintes formatos:  .jpg, .png, .pdf, .doc ou .html;</li>
						<li>• Aceitamos somente comprovantes com no máximo 5 dias após pagamento, clientes que enviarem comprovantes após este período;</li>não terão seu login liberado;
						<li>• O arquivo enviado deve ter um tamanho máximo de 3MB;</li>
						<li>• Não serão aceitos comprovantes ilegíveis;</li>
						<li>• Em caso de pagamentos por depósito ou transferência é OBRIGATÓRIO comprovante de compra como FOTO ou IMAGEM;</li>
						<li>• O prazo de liberação após envio do comprovante pelo nosso site é de até 24hrs. Favor sempre aguardar este prazo.</li>				
					</ul>
				</div>					

			</div>				

		</div>
	</section>

	<section class="nossos-diferenciais">
		<div class="container">

			<h2 class="title-1 text-center">
				Nossos <span>Diferenciais</span>
			</h2>

			<ul class="owl-carousel owl-theme diferencial">
				<li class="item item-1 text-center">
					<h3>Estabilidade Comprovada no Teste CS</h3>
					<p>Nossa missão é manter o servidor estável e sem travas pra você desfrutar do melhor dos nossos canais Claro TV, Sky e Net HD.</p>
					<div class="efeito efeito-1"></div>
					<div class="efeito efeito-2"></div>
					<div class="efeito efeito-3"></div>
				</li>
				<li class="item item-2 text-center">
					<h3>Preços Justos e Qualidade Garantida</h3>
					<p>Melhores preços CS Claro TV, Sky e Net HD. Você paga o justo e sem abusos nos preços. Consulte nossos planos.</p>
					<div class="efeito efeito-1"></div>
					<div class="efeito efeito-2"></div>
					<div class="efeito efeito-3"></div>					
				</li>
				<li class="item item-3 text-center">
					<h3>Atendimento Profissional com Equipe Preparada</h3>
					<p>Nossa equipe sempre estará pronta pra lhe atender da melhor maneira possível com um suporte claro e eficiente.</p>
					<div class="efeito efeito-1"></div>
					<div class="efeito efeito-2"></div>
					<div class="efeito efeito-3"></div>					
				</li>										
			</ul>

			<p class="quero-ver-mais">
				<i class="fas fa-angle-double-left"></i>
					Deslize para ver mais.
				<i class="fas fa-angle-double-right"></i>
			</p>

		</div>
	</section>

<?php require_once('includes/blog-feed.php') ?>	

<?php require_once('includes/footer.php') ?>	

