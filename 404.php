<?php
	$titulo = "Página não encontrada | CS SAT Soluções em TVs";
	$descricao = "Página não encontrada";
	$page = "index";
	$canonical = "https://www.cssat.com.br/404.php";	

	require_once('includes/header.php');
?>

<section class="thank-you">
	<div class="container">
		<div class="row">
			<h1 class="title-1 text-center">Página não encontrada</h1>
			<p class="text-center">Entre em contato e tire suas dúvidas.</p>
			<p> <a href="index.php" class="bt-2 text-center">Ou volte para a Home</a></p>
		</div>
	</div>
</section>

<?php require_once('includes/footer-thank-you.php') ?>	

