<?php
	$titulo = "Cardsharing | CS SAT Soluções em TVs";
	$descricao = "Saiba o que é Cardsharing, como utilizar e quais os preços para contratação do seu CS.";
	$page = "internas";
	$canonical = "https://www.cssat.com.br/cardsharing.php";

	require_once('includes/header.php');
?>

<section class="descritivo">
    <div class="container">
        <div class="col-md-12">
            <div class="col-lg-7 col-md-12">
                <h1 class="title-1 text-left">
                    O que é <span>Cardsharing</span>
                </h1>
            </div>
            <div class="col-lg-12 col-md-12">
                <picture>
                    <source media="(max-width: 768px)" srcset="images/cs-claro-mobile.png">
                    <img src="images/cs-claro.png" width="546" height="582" alt="Cardsharing Claro" title="Cardsharing Claro">
                </picture>
            </div>
            <div class="col-lg-7 col-md-12">
                <p><b>Cardsharing</b> ou <b>CS</b> é o nome dado para o compartilhamento de cartões codificados de operadoras de TV a cabo através da internet. Muito popular no Brasil e Europa, o Cardsharing surgiu como uma solução para quem deseja assistir TV a cabo com um baixo custo. Cardsharing, também conhecido pela sigla CS, é diferente de IPTV e compartilhamento de imagens de canais. O serviço é focado em decodificação do cartão.</p>
                <h2>Como funciona Cardsharing (CS)?</h2>
                <p>Um cartão codificado é inserido no receptor da operadora escolhida e esse receptor opera como um servidor para que outros receptores conectados à internet recebam os dados, simulando como se o receptor também estivesse utilizando o cartão original.</p>
                <p>Através de um único receptor servidor é possível compartilhar centenas de canais de Tv por assinatura por um preço bem menor do que o normal, embora antes de aderir o sistema Cardsharing você deve se atentar ao a marca e modelo do seu receptor, pois nem todos receptores conseguem funcionar em CS.</p>
                <p>Pois a condição principal que permite o uso desse sistema é o receptor ter conexão com a internet, os receptores mais conhecidos são os Dreambox, Azamérica S900HD, AzamericaS1001, Azbox Elite HD, Alphasat Chroma e Azbox Premium HD, para isso é necessário o aparelho ter uma entrada RJ-45.
                </p>
                <p>Além disso, cada receptor precisa possuir um firmware especifico e um emulador que faz com que os receptores se comuniquem entre si, é importante ressaltar que os receptores compatíveis com o Cardsharing são FTA normais e legais e só podem funcionar em CS quando é alterado o seu o firmware original.</p>
                <p>E o preço? Bom, você pode conferir o preço do CS logo abaixo.</p>
            </div>
        </div>

    </div>
</section>

<section class="nossos-planos">
    <div class="container">

        <div class="col-md-8 col-md-offset-2 col-sm-12 col-sm-offset-0">
            <h2 class="title-1 text-center">
                Conheça <span>Nossos Planos</span>
            </h2>
            <p class="text-center">Temos planos para as operadoras Claro, Sky e Net, com vantagens para o plano mensal, bimestral, trimestral ou semestral. <b>Faça o teste de cs grátis e descubra a melhor opção para você!</b></p>
        </div>

        <div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">

            <div class="list">
                <p>
                    <i class="bt mensal sprite sprite-radio-disable active"></i>Mensal
                </p>
                <p>
                    <i class="bt bimestral sprite sprite-radio-disable"></i>Trimestral
                </p>
                <p class="trimestral-label">
                    <i class="bt trimestral sprite sprite-radio-disable"></i>Semestral
                </p>
                <p>
                    <i class="bt semestral sprite sprite-radio-disable"></i>Anual
                </p>
            </div>

            <ul class="owl-carousel owl-theme plano plano-1">
                <li class="item text-center">
                    <a href="cs-claro.php" title="Ir para CS Claro">
                        <img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
                    </a>
                    <h3 class="title-plan">Teste CS Claro</h3>
                    <p class="valor"><span>R$</span>15.00</p>
                    <ul>
                        <li>Operadora Claro TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-claro-M teste-claro" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-sky.php" title="Ir para CS SKY">
                        <img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
                    </a>
                    <h3 class="title-plan">Teste CS Sky</h3>
                    <p class="valor"><span>R$</span>15.00</p>
                    <ul>
                        <li>Operadora SKY TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-sky-M teste-sky" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-net.php" title="Ir para CS NET">
                        <img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
                    </a>
                    <h3 class="title-plan">Teste CS Net</h3>
                    <p class="valor"><span>R$</span>20.00</p>
                    <ul>
                        <li>Operadora NET TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-net-M teste-net" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
            </ul>

            <ul class="owl-carousel owl-theme plano plano-2">
                <li class="item text-center">
                    <a href="cs-claro.php" title="Ir para CS Claro">
                        <img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
                    </a>
                    <h3 class="title-plan">Teste CS Claro</h3>
                    <p class="valor"><span>R$</span>45.00</p>
                    <ul>
                        <li>Operadora Claro TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-claro-T teste-claro" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-sky.php" title="Ir para CS SKY">
                        <img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
                    </a>
                    <h3 class="title-plan">Teste CS Sky</h3>
                    <p class="valor"><span>R$</span>45.00</p>
                    <ul>
                        <li>Operadora SKY TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-sky-T teste-sky" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-net.php" title="Ir para CS NET">
                        <img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
                    </a>
                    <h3 class="title-plan">Teste CS Net</h3>
                    <p class="valor"><span>R$</span>60.00</p>
                    <ul>
                        <li>Operadora NET TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-net-T teste-net" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
            </ul>

            <ul class="owl-carousel owl-theme plano plano-3">
                <li class="item text-center">
                    <a href="cs-claro.php" title="Ir para CS Claro">
                        <img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
                    </a>
                    <h3 class="title-plan">Teste CS Claro</h3>
                    <p class="valor"><span>R$</span>80.00</p>
                    <ul>
                        <li>Operadora Claro TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-claro-S teste-claro" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-sky.php" title="Ir para CS SKY">
                        <img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
                    </a>
                    <h3 class="title-plan">Teste CS Sky</h3>
                    <p class="valor"><span>R$</span>80.00</p>
                    <ul>
                        <li>Operadora SKY TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-sky-S teste-sky" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-net.php" title="Ir para CS NET">
                        <img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
                    </a>
                    <h3 class="title-plan">Teste CS Net</h3>
                    <p class="valor"><span>R$</span>110.00</p>
                    <ul>
                        <li>Operadora NET TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-net-S teste-net" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
            </ul>

            <ul class="owl-carousel owl-theme plano plano-4">
                <li class="item text-center">
                    <a href="cs-claro.php" title="Ir para CS Claro">
                        <img src="images/logo-claro.png" width="153" height="137" alt="Valor CS Claro" title="Ir para CS Claro">
                    </a>
                    <h3 class="title-plan">Teste CS Claro</h3>
                    <p class="valor"><span>R$</span>155.00</p>
                    <ul>
                        <li>Operadora Claro TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-claro-A teste-claro" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-sky.php" title="Ir para CS SKY">
                        <img src="images/logo-sky.png" width="153" height="137" alt="Valor CS SKY" title="Ir para CS SKY">
                    </a>
                    <h3 class="title-plan">Teste CS Sky</h3>
                    <p class="valor"><span>R$</span>155.00</p>
                    <ul>
                        <li>Operadora SKY TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-sky-A teste-sky" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
                <li class="item text-center">
                    <a href="cs-net.php" title="Ir para CS NET">
                        <img src="images/logo-net.png" width="153" height="137" alt="Valor CS NET" title="Ir para CS NET">
                    </a>
                    <h3 class="title-plan">Teste CS Net</h3>
                    <p class="valor"><span>R$</span>210.00</p>
                    <ul>
                        <li>Operadora NET TV</li>
                        <li>Grade Completa de Canais</li>
                        <li>Mais de 500 Canais</li>
                        <li>Servidor Dedicado e Exclusivo</li>
                        <li>Suporte 7 dias por semana</li>
                        <li>Teste 24 Horas</li>
                    </ul>
                    <a class="bt-2 teste-net-A teste-net" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!">Teste aqui</a>
                </li>
            </ul>

            <p class="quero-ver-mais">
                <i class="fas fa-angle-double-left"></i>
                Deslize para ver mais.
                <i class="fas fa-angle-double-right"></i>
            </p>

            <a class="bt-3 teste-planos" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" rel="nofollow noopener" target="_blank" title="Faça um teste agora!"><i class="fas fa-play"></i>Quero Testar Grátis!</a>
        </div>

    </div>
</section>

<section class="receptores">
    <div class="container">

        <div class="col-md-12">
            <h2 class="title-1 text-center">Receptadores<span>Suportados</span></h2>
            <p class="text-center">Você terá acesso a canais de tv fechado e aberto para os receptores abaixo.</p>
        </div>

        <ul class="owl-carousel owl-theme lista-receptores">
            <li class="item">
                <img src="images/receptores/r1.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r2.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r3.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r4.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r5.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r6.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r7.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r8.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r9.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r10.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r11.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r12.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r13.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r14.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r15.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r16.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r17.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
            <li class="item">
                <img src="images/receptores/r18.jpg" width="167" height="46" alt="Receptor Cardsharing" title="Receptor Cardsharing">
            </li>
        </ul>

        <p class="quero-ver-mais">
            <i class="fas fa-angle-double-left"></i>
            Deslize para ver mais.
            <i class="fas fa-angle-double-right"></i>
        </p>

    </div>
</section>

<?php require_once('includes/blog-feed.php') ?>

<?php require_once('includes/footer.php') ?>
