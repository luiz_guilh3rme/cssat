User-agent: Googlebot-Image
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~

User-agent: Mediapartners-Google
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~

User-agent: Adsbot-Google
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~

User-agent: Googlebot-Mobile
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~

User-agent: Googlebot
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~



User-agent: Bingbot
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~


User-agent: Slurp
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~


User-agent: DuckDuckBot
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~

User-agent: Baiduspider
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~


User-agent: YandexBot
Allow: 
Disallow: /?s=
Disallow: /tag/
Disallow: /category/
Disallow: /wp-admin/
Disallow: /wp-content/
Disallow: /wp-admin/admin-ajax.php
Disallow: /wp-admin/
Disallow: /wp-includes/
Disallow: /readme.html
Disallow: /license.txt
Disallow: /xmlrpc.php
Disallow: /wp-login.php
Disallow: /wp-register.php
Disallow: /*?*
Disallow: /*?
Disallow: /*~*
Disallow: /*~



Sitemap: https://www.cssat.com.br/sitemap.xml