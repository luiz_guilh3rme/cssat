<?php
	$titulo = "Teste CS NET | CS SAT Soluções em TVs";
	$descricao = "O Teste CS NET disponibiliza a programação completa da operadora, além de suporte sempre que precisar ou tiver dúvidas por meio do chat disponível no site.";
	$page = "internas";
	$canonical = "https://www.cssat.com.br/cs-net.php";	

	require_once('includes/header.php');
?>

<section class="descritivo">
	<div class="container">
		<div class="col-md-12">
			<div class="col-lg-7 col-md-12">				
				<h1 class="title-1 text-left">
					CS <span>NET</span>
				</h1>
			</div>
			<div class="col-lg-12 col-md-12">				
				<picture>
				  <source media="(max-width: 768px)" srcset="images/cs-net-mobile.png">
				  <img src="images/webp/cs-net.webp" width="546" height="582" alt="Teste CS NET" title="Teste CS NET">
				</picture>	
			</div>
			<div class="col-lg-7 col-md-12">	
				<p>Ao assinar o plano Teste CS NET, o usuário tem acesso a mais de 500 canais em Full HD, próprios da operadora NET. Além disso, ele recebe suporte de profissionais preparados e capacitados para esclarecer dúvidas ou auxiliar a usufruir dos serviços.</p>						
				<p>Com esse plano, o cliente tem acesso a um servidor com sinal amplamente estável e qualidade de imagem. Para adquirir, basta ter acesso a internet e realizar o pagamento pelo Pag Seguro para um dos planos: mensal, bimestral, trimestral e semestral.</p>
				<p>O Teste CS NET funciona da seguinte forma: através de um servidor primário, o sinal é compartilhado para outros servidores que possuem um cartão CS, desta forma, você terá acesso a todos os canais disponíveis na NET, para acessar pela TV, celular, tablet e outros dispositivos.</p>
				<p>Para quem estiver em dúvida sobre qual plano adquirir, o cliente poderá fazer um teste válido por 24 horas.</p>
				<p>Conheça os nossos planos e faça o teste agora mesmo!</p>
			</div>
		</div>

	</div>
</section>

<section class="nossos-planos internas">
	<div class="container">

		<div class="col-md-8">

			<h2 class="title-1 color-2">
				Conheça <span>Nossos Planos</span>
			</h2>
			<p class="color-2">Veja as opções para as operadoras Claro, Sky e Net, em planos mensal, bimestral, trimestral ou semestral. O pagamento é feito de forma rápida e fácil, com um valor que cabe no seu bolso, além de exibição de mais de 500 canais em altíssima qualidade. Comprove fazendo o teste gratuitamente!</p>

			<div class="list">
				<p>
					<i class="bt mensal sprite sprite-radio-disable active"></i>Mensal
				</p>
				<p>
					<i class="bt bimestral sprite sprite-radio-disable"></i>Trimestral
				</p>
				<p class="trimestral-label">
					<i class="bt trimestral sprite sprite-radio-disable"></i>Semestral
				</p>
				<p>
					<i class="bt semestral sprite sprite-radio-disable"></i>Anual
				</p>
			</div>	
			
			<a class="bt-3 hidden-xs teste-net" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!"><i class="fas fa-play"></i>Quero Testar Grátis!</a>

		</div>

		<div class="col-md-4">
			<ul class="plano plano-1">
				<li class="item text-center">
					<img src="images/logo-net.png" width="153" height="137" alt="NET" title="NET">
					<h3 class="title-plan">Plano NET</h3>
					<p class="valor"><span>R$</span>20.00</p>
					<ul>
						<li>Operadora NET TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul>					
				</li>
			</ul>

			<ul class="plano plano-2">
				<li class="item text-center">
					<img src="images/logo-net.png" width="153" height="137" alt="NET" title="NET">
					<h3 class="title-plan">Plano NET</h3>
					<p class="valor"><span>R$</span>60.00</p>
					<ul>
						<li>Operadora NET TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul>					
				</li>
			</ul>

			<ul class="plano plano-3">
				<li class="item text-center">
					<img src="images/logo-net.png" width="153" height="137" alt="NET" title="NET">
					<h3 class="title-plan">Plano NET</h3>
					<p class="valor"><span>R$</span>110.00</p>
					<ul>
						<li>Operadora NET TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul					
				</li>
			</ul>

			<ul class="plano plano-4">
				<li class="item text-center">
					<img src="images/logo-net.png" width="153" height="137" alt="NET" title="NET">
					<h3 class="title-plan">Plano NET</h3>
					<p class="valor"><span>R$</span>210.00</p>
					<ul>
						<li>Operadora NET TV</li>
						<li>Grade Completa de Canais</li>
						<li>Mais de 500 Canais</li>
						<li>Servidor Dedicado e Exclusivo</li>
						<li>Suporte 7 dias por semana</li>
						<li>Teste 24 Horas</li>
					</ul>					
				</li>
			</ul>
		</div>

		<div class="col-xs-12 hidden-lg hidden-md hidden-sm">
			<a class="bt-3" href="teste.php" rel="nofollow noopener" target="_blank" alt="Faça um teste agora" title="Faça um teste agora!"><i class="fas fa-play"></i>Quero Testar Grátis!</a>
		</div>

	</div>
</section>

<section class="receptores">
	<div class="container">

		<div class="col-md-12">
			<h2 class="title-1 text-center">Receptadores<span>Suportados</span></h2>
			<p class="text-center">Você terá acesso a canais de tv fechado e aberto para os receptores abaixo.</p>
		</div>

		<ul class="owl-carousel owl-theme lista-receptores">
			<li class="item">
				<img src="images/receptores/r1.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r2.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r3.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r4.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r5.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r6.jpg" width="167" height="46" alt="" title="">
			</li>	
			<li class="item">
				<img src="images/receptores/r7.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r8.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r9.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r10.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r11.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r12.jpg" width="167" height="46" alt="" title="">
			</li>	
			<li class="item">
				<img src="images/receptores/r13.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r14.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r15.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r16.jpg" width="167" height="46" alt="" title="">
			</li>
			<li class="item">
				<img src="images/receptores/r17.jpg" width="167" height="46" alt="" title="">
			</li>		
			<li class="item">
				<img src="images/receptores/r18.jpg" width="167" height="46" alt="" title="">
			</li>																						
		</ul>

		<p class="quero-ver-mais">
			<i class="fas fa-angle-double-left"></i>
				Deslize para ver mais.
			<i class="fas fa-angle-double-right"></i>
		</p>

	</div>
</section>

<?php require_once('includes/footer.php') ?>	

