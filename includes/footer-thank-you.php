	<footer>
		<div class="container">
			<div class="row">									
				<div class="col-md-4">
					<p class="sobre">A <b>CS SAT</b> hoje é considerada a melhor empresa de cardsharing do Brasil, com central de atendimento fixa. Fornece uma linha completa de produtos como planos de TV, <b>CS NET</b>, <b>CS Sky</b> e <b>CS Claro</b> por meio de servidores dedicados CS online para manter seu equipamento funcionando sem travas.</p>
					<p class="sobre">Trabalhamos há <b>5 anos</b> no mercado com suporte e serviço on-line para todo o Brasil.</p>
				</div>
				<div class="col-md-2 hidden-xs">
					<nav>
						<ul>
							<li><a href="index.php" title="Visite a Home"><i class="fas fa-long-arrow-alt-right"></i>Home</a></li>
							<li><a href="http://144.217.7.178/painel/testar.php?f=VkZaRk9WQlJQVDA9" target="_blank" title="Faça um teste grátis"><i class="fas fa-long-arrow-alt-right"></i>Teste Grátis</a></li>
							<!-- <li><a href="#"><i class="fas fa-long-arrow-alt-right"></i></i>Comprar</a></li> -->
							<li><a href="./blog" target="_blank" title="Visite nosso Blog"><i class="fas fa-long-arrow-alt-right"></i>Blog</a></li>
							<li><a href="#contact" title="Entre em contato conosco"><i class="fas fa-long-arrow-alt-right"></i>Contato</a></li>
						</ul>
					</nav>
				</div>
				<div class="blog col-md-3 hidden-xs">
					<h2 class="title-3">Últimas do Blog</h2>
					<ul>
						<?php require_once('includes/blog-feed-footer.php'); ?>				
					</ul>					
				</div>
				<div class="col-md-3 hidden-xs">
					<h2 class="title-4">Redes Sociais</h2>
					<ul>
						<li><a class="social" href="#" target="_blank" title="Visite nossa página no Facebook" ><i class="fab fa-facebook-f"></i></a></li>
						<li><a class="social" href="#" target="_blank" title="Visite nossa página no Instagram" ><i class="fab fa-instagram"></i></a></li>
					</ul>
					<a class="bt-4" href="http://144.217.7.178/painel/login.php" target="_blank" title="Acesse sua area de Cliente aqui!">Painel do Cliente</a>
				</div>
				<div class="line col-md-12">
					<p class="copy"><b>CS SAT © 2018</b> - Todos os direitos reservados.</p>
					<a href="https://www.3xceler.com.br/criacao-de-sites" target="_blank" title="Criação de Sites" class="developer">
						Criação de Sites
						<img src="images/logo-3xceler.png" width="90" height="27" alt="3xceler" title="3xceler">
					</a>
				</div>
			</div>
		</div>
	</footer>

	<div class="whatsapp">
		<img class="close-whats" src="images/close.png" width="9" height="9" alt="Fechar" title="Fechar">
		<a href="./wpp.php" title="Fale conosco no Whatsapp!" target="blank" >
			<img src="images/caixa-whats.png" width="246" height="194" alt="Envie um Whatsapp" title="Envie um Whatsapp">
		</a>
	</div>

	<div class="faca-um-teste hidden-lg hidden-md hidden-sm">
		<a href="http://144.217.7.178/painel/testar.php?f=VkZaRk9WQlJQVDA9" target="_blank">FAÇA UM TESTE GRÁTIS</a>
	</div>	

</main>
	
</body>

<!-- JS -->
<script src="dist/app.js"></script>


</html>


