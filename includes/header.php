<?php include("includes/mail.php"); ?>

<!DOCTYPE html>
<html lang="pt-bt">
<head>
	<meta charset="UTF-8">
	<meta author="3xceler">
	<meta name="description" content="<?= $descricao ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta property="og:description" content="<?= $descricao ?>">
    <meta property="og:url" content="<?= $canonical ?>" />
    <meta name="theme-color" content="#fe044b"/>
	<link rel="canonical" href="<?= $canonical ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#fe044b">
    <meta name="msapplication-TileColor" content="#fe044b">
    <meta name="theme-color" content="#fe044b">

	<!-- CSS -->
	<link rel="stylesheet" href="dist/main.css">

	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">	

	<!-- FONT -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	
	<link rel="stylesheet" href="dist/bootstrap.css">

	<title><?= $titulo ?></title>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5XB58P6');</script>
<!-- End Google Tag Manager -->
    
<!--Start of Tawk.to Script
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5b733d0bafc2c34e96e7945e/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
--End of Tawk.to Script-->
</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5XB58P6"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<header id="header">

		<nav class="navbar navbar-default">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false" aria-label="navbar" role="none" aria-hidden="true">
					</button>
					<a class="navbar-brand" href="index.php">
						<picture>
						  <source media="(max-width: 768px)" srcset="images/logo-mobile.png">
						  <img src="images/logo.png" alt="CS SAT Soluções em TVs" title="CS SAT Soluções em TVs">
						</picture>
					</a>
				</div>

				<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

					<a class="bt-0 hidden-xs" href="painel.php" rel="nofollow noopener" target="_blank" title="Acesse sua area de Cliente aqui!"><span>Painel do Cliente</span></a>

					<ul class="nav navbar-nav navbar-right">
						<li class="active"><a href="index.php" title="Visite a Home"><i class="fas fa-long-arrow-alt-right"></i> HOME</a></li>
						<li><a href="teste.php" rel="nofollow noopener" target="_blank" title="Faça um teste grátis" class="teste-menu gtm-menu"><i class="fas fa-long-arrow-alt-right"></i>TESTE GRÁTIS</a></li>
						<li><a href="./cs-claro.php" title="Teste CS 24h Claro" class="teste-menu"><i class="fas fa-long-arrow-alt-right"></i>CLARO</a></li>
						<li><a href="./cs-net.php" title="Teste CS 24h NET" class="teste-menu"><i class="fas fa-long-arrow-alt-right"></i>NET</a></li>
						<li><a href="./cs-sky.php" title="Teste CS 24h NET" class="teste-menu"><i class="fas fa-long-arrow-alt-right"></i>SKY</a></li>
						<!-- <li><a href="#"><i class="fas fa-long-arrow-alt-right"></i></i> COMPRAR</a></li> -->
						<li><a href="./blog" title="Visite nosso Blog"><i class="fas fa-long-arrow-alt-right"></i> BLOG</a></li>
						<li><a href="#contact" title="Entre em contato conosco"><i class="fas fa-long-arrow-alt-right"></i> CONTATO</a></li>
						<!-- <li class="hidden-xs"><a class="social" href="#" target="_blank" title="Visite nossa página no Facebook" ><i class="fab fa-facebook-f"></i></a></li> -->
						<!-- <li class="hidden-xs"><a class="social" href="#" target="_blank" title="Visite nossa página no Instagram" ><i class="fab fa-instagram"></i></a></li> -->
					</ul>

					<ul class="social-mobile">
						<li><a class="bt-0" href="painel.php" rel="nofollow noopener" target="_blank"><span>Painel do Cliente</span></a></li>
						<!-- <li><a class="social" href="#"><i class="fab fa-facebook-f"></i></a></li> -->
						<!-- <li><a class="social" href="#"><i class="fab fa-instagram"></i></a></li> -->
					</ul>
				</div>
			</div>
		</nav>
	</header>

	<main>