<?php
function get_recent_posts($per_page = 6) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/blog/wp-json/wp/v2/posts?per_page={$per_page}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if( is_array($result) && !empty($result) ) {
		for( $i = 0; $i < count($result); $i++ ) {
			$result[$i]['featured_image_url'] = get_media( $result[$i]["featured_media"] );
		}
		return $result;
	}

	else {

		return false;
	}
};

function get_media($id) {

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/blog/wp-json/wp/v2/media/{$id}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if ( is_array($result) && !empty($result) ) {
		if ( isset( $result["media_details"] ) ) {
			return $result["media_details"]["sizes"]["medium"]["source_url"];
		}
	}

	else {
		return false;
	}
};


$posts = get_recent_posts(6);

if ($posts) {
	?>


	<section class="nosso-blog">
		<div class="container">
			<div class="col-lg-10 col-lg-offset-1 col-md-12 col-md-offset-0">
				<h2 class="title-1 color-2 text-center">
					Últimas do <span>Nosso Blog</span>
				</h2>
				<p class="text-center color-2">Novidades, notícias e atualizações sobre o CS TV e lançamentos de séries, filmes e programas da televisão fechada. Fique por dentro de nosso blog!</p>

				<ul class="owl-carousel owl-theme artigos">
				<?php 
				foreach($posts as $post) {
				$date = new DateTime($post['date_gmt']);
				?>
					<li class="item text-center">
			    		<a class="link-artigo" href="<?= $post['link'];  ?>" title="<?= $post['title']['rendered']; ?>"></a>								
						<h3 class="title"><?= $post['title']['rendered']; ?></h3>
						<p class="data">
							<i class="fas fa-calendar-alt"></i>
							<?= $date->format("d"); ?> de <?= $date->format("M"); ?> de <?= $date->format("Y"); ?>
						</p>
						<p class="autor"><i class="fas fa-user"></i>Postado por Admin</p>
						<p class="txt">
							<?php 
								ob_start();
								echo $post["content"]["rendered"];
								$old_content = ob_get_clean();
								$new_content = strip_tags($old_content);
								$content_excerpt = substr($new_content, 0 ,120).'...';
								echo $content_excerpt; 
							?>
						</p>
						<img src="<?= $post["featured_image_url"] ?>" alt="<?= $post['title']['rendered']; ?>" title="<?= $post['title']['rendered']; ?>">
					</li>
				<?php 
				}
				?>
				</ul>

				<p class="quero-ver-mais">
					<i class="fas fa-angle-double-left"></i>
						Deslize para ver mais.
					<i class="fas fa-angle-double-right"></i>
				</p>					

			</div>
		</div>
	</section>

<?php } ?>

</section>	