	<section class="depoimentos">
		<div class="container-fluid">
			<div class="col-md-8 col-md-offset-2">
				<h2 class="title-1 text-center">
					O QUE DIZEM <span>NOSSOS CLIENTES</span>
				</h2>
				<p class="text-center">Quer saber mais sobre o desempenho de nossos serviços? <br> Confira a opinião de nossos clientes que adquiriram e comprovaram a qualidade de nossos planos!</p>
			</div>

			<ul class="grupo col-md-12 owl-carousel owl-theme">	        					
			    <li class="item">
			    	<img src="images/foto-depoimento-1.png" width="90" height="90" alt="" title="">
					<p class="txt">“Conhecemos a CS SAT através de um cliente e desde então utilizamos seus produtos. Além da qualidade nos produtos, o prazo de entrega são excelentes.”</p>
					<p class="autor text-center">Ana Paula Rodrigues Silva</p>
			    </li>		
			    <li class="item">
			    	<img src="images/foto-depoimento-2.png" width="90" height="90" alt="" title="">
					<p class="txt">"Tive o prazer de trabalhar com CS SAT e fui muito bem atendido desde o primeiro contato até a instalação, é sempre bom saber que temos parceiros competentes e que se preocupam com o Cliente."</p>
					<p class="autor text-center">José Gama Oliveira</p>
			    </li>	
			    <li class="item">
			    	<img src="images/foto-depoimento-3.png" width="90" height="90" alt="" title="">
					<p class="txt">"A empresa CS SAT atendeu as expectativas em relação aos serviços contratados com qualidade e pontualidade."</p>
					<p class="autor text-center">Anérita Campos Bernardes</p>
			    </li>	
			    <li class="item">
			    	<img src="images/foto-depoimento-4.png" width="90" height="90" alt="" title="">
					<p class="txt">"Sou grato ao CS SAT. Nas instalações sempre foram muito educados, corretos e discretos. Deixaram o local limpo e foram caprichosos."</p>
					<p class="autor text-center">Angelo Soares Guimarães</p>
			    </li>				    			    			    			    	    
			</ul>

			<p class="quero-ver-mais">
				<i class="fas fa-angle-double-left"></i>
					Deslize para ver mais. 
				<i class="fas fa-angle-double-right"></i>
			</p>				

		</div>
	</section>

	<section id="contact" class="contato <?= $page ?>">
		<div class="container">
			<div class="row">					
				<div class="col-md-7">
					<h2 class="title-1">
						Entre em <span>Contato</span>
					</h2>
					<p>Disponibilizamos canais exclusivos para atendimento de nossos clientes. Preencha o formulário, ou se preferir utilize outro canal de contato abaixo.</p>
				</div>
			</div>
			<div class="row">					
				<div class="col-lg-3 col-md-4">
					<h3 class="title-2">Tem alguma dúvida? <span>Entre em Contato</span></h3>	
					<p class="tel">
						<i class="fab fa-whatsapp"></i>
                        <a href="./wpp.php" rel="nofollow noopener" class="whats-footer" title="Fale conosco no Whatsapp!" target="_blank" >38 99804.4670</a><br>
                        <a href="./wpp2.php" rel="nofollow noopener" class="whats-footer" title="Fale conosco no Whatsapp!"  target="_blank" >38 99947.1683</a>
					</p>				
				</div>
				<div class="col-lg-3 col-md-4">
					<h3 class="title-2">Envie um <span>E-mail</span></h3>	
					<p class="email">
						<i class="far fa-envelope"></i>
						cssatbr@gmail.com
					</p>										
				</div>					
			</div>
			<div class="row">					
				<div class="col-lg-6 col-md-8">									

					<form class="formulario" action="<? $PHP_SELF; ?>" method="POST">
						<div class=" form-group col-md-6">
							<input class="form-control" type="text" name="user-name" aria-label="Nome" placeholder="Nome:" required>
						</div>		
						<div class=" form-group col-md-6">
							<input class="form-control" type="email" name="user-email" aria-label="Email" placeholder="E-mail:" required>
						</div>		
						<div class=" form-group col-md-6">
							<input class="form-control" type="tel" name="user-phone" aria-label="Telefone" placeholder="Telefone:" required>
						</div>		
						<div class=" form-group col-md-6">
							<select class="form-control" name="user-subject" aria-label="Assunto" required>
								<option disabled selected>Assunto:</option>
								<option value="Dúvida">Dúvida</option>
								<option value="Sugestão">Sugestão</option>
								<option value="Suporte">Suporte</option>
							</select>
						</div>		
						<div class=" form-group col-md-12">
							<textarea class="form-control" name="user-message" aria-label="Mensagem" placeholder="Mensagem:" required></textarea>
							<button class="enviar"><i class="fab fa-telegram-plane"></i><span>Enviar</span></button>
						</div>																				
					</form>

				</div>
			</div>
		</div>
	</section>


	<footer>
		<div class="container">
			<div class="row">									
				<div class="col-md-4">
					<p class="sobre">A <b>CS SAT</b> hoje é considerada a melhor empresa de cardsharing do Brasil, com central de atendimento fixa. Fornece uma linha completa de produtos como planos de TV, <b>Teste CS NET</b>, <b>Teste CS SKY</b> e <b>Teste CS Claro</b> por meio de servidores dedicados CS online para manter seu equipamento funcionando sem travas.</p>
					<p class="sobre">Trabalhamos há <b>5 anos</b> no mercado com suporte e serviço on-line para todo o Brasil.</p>
				</div>
				<div class="col-md-2 hidden-xs">
					<nav>
						<ul>
							<li><a href="index.php" title="Visite a Home"><i class="fas fa-long-arrow-alt-right"></i>Home</a></li>
							<li><a href="./cs-claro.php" title="Teste CS Claro"><i class="fas fa-long-arrow-alt-right"></i>Teste CS Claro</a></li>
							<li><a href="./cs-net.php" title="Teste CS NET"><i class="fas fa-long-arrow-alt-right"></i>Teste CS NET</a></li>
							<li><a href="./cs-sky.php" title="Teste CS SKY"><i class="fas fa-long-arrow-alt-right"></i>Teste CS SKY</a></li>
							<!-- <li><a href="#"><i class="fas fa-long-arrow-alt-right"></i></i>Comprar</a></li> -->
							<li><a href="./blog" target="_blank" title="Visite nosso Blog"><i class="fas fa-long-arrow-alt-right"></i>Blog</a></li>
							<li><a href="#contact" title="Entre em contato conosco"><i class="fas fa-long-arrow-alt-right"></i>Contato</a></li>
						</ul>
					</nav>
				</div>
				<div class="blog col-md-3 hidden-xs">
					<h2 class="title-3">Últimas do Blog</h2>
					<ul>
						<?php require_once('includes/blog-feed-footer.php'); ?>				
					</ul>					
				</div>
				<div class="col-md-3 hidden-xs">
					<!-- <h2 class="title-4">Redes Sociais</h2> -->
					<ul>
						<!-- <li><a class="social" href="#" target="_blank" title="Visite nossa página no Facebook" ><i class="fab fa-facebook-f"></i></a></li> -->
						<!-- <li><a class="social" href="#" target="_blank" title="Visite nossa página no Instagram" ><i class="fab fa-instagram"></i></a></li> -->
					</ul>
					<a class="bt-4" href="painel.php" rel="nofollow noopener" target="_blank" title="Acesse sua area de Cliente aqui!">Painel do Cliente</a>
				</div>
				<div class="line col-md-12">
					<p class="copy"><b>CS SAT © 2018</b> - Todos os direitos reservados.</p>
					<a href="https://www.3xceler.com.br/criacao-de-sites" target="_blank" rel="noopener" title="Criação de Sites" class="developer">
						Criação de Sites
						<img src="images/logo-3xceler.png" width="90" height="27" alt="3xceler" title="3xceler">
					</a>
				</div>
			</div>
		</div>
	</footer>

	<div class="whatsapp">
		<img class="close-whats" src="images/close.png" width="9" height="9" alt="Fechar" title="Fechar">
		<a href="./wpp.php" rel="nofollow noopener" title="Fale conosco no Whatsapp!" target="_blank">
			<img src="images/caixa-whats.png" width="246" height="194" alt="Envie um Whatsapp" title="Envie um Whatsapp">
		</a>
	</div>

	<div class="faca-um-teste hidden-lg hidden-md hidden-sm">
		<a class="Teste-mobile" href="http://www1.cssat.com.br/painel/testar.php?f=VkZaRk9WQlJQVDA9" title="Faça um teste" rel="nofollow noopener" target="_blank">
			<i class="fas fa-play"></i>
			<span>FAÇA UM TESTE GRÁTIS</span>
		</a>
		<a class="whats" href="https://api.whatsapp.com/send?phone=5538998044670" rel="nofollow noopener" title="Fale conosco no Whatsapp!" class="whats-footer" target="_blank">
			<i class="fab fa-whatsapp"></i>
			<span>FALE PELO WHATSAPP</span>
		</a>
	</div>	

</main>
	
</body>

<!-- JS -->
<script src="dist/app.js"></script>


</html>


