<?php
function get_recent_posts_footer($per_page = 2) {
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/blog/wp-json/wp/v2/posts?per_page={$per_page}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if( is_array($result) && !empty($result) ) {
		for( $i = 0; $i < count($result); $i++ ) {
			$result[$i]['featured_image_url'] = get_media_footer( $result[$i]["featured_media"] );
		}
		return $result;
	}

	else {

		return false;
	}
};

function get_media_footer($id) {

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $_SERVER['SERVER_NAME'] . "/blog/wp-json/wp/v2/media/{$id}");
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

	$output = curl_exec($ch);
	$result = json_decode($output, true);
	curl_close($ch);

	if ( is_array($result) && !empty($result) ) {
		if ( isset( $result["media_details"] ) ) {
			return $result["media_details"]["sizes"]["medium"]["source_url"];
		}
	}

	else {
		return false;
	}
};


$posts_footer = get_recent_posts_footer();

?>

<?php

	if ($posts_footer) {
		foreach($posts_footer as $post) {
		$date = new DateTime($post['date_gmt']);
?>			

		<li>
			<a href="#"><?= $post['title']['rendered']; ?></a>
			<p class="data">
				<i class="fas fa-calendar-alt"></i>
				<?= $date->format("d"); ?> de <?= $date->format("M"); ?> de <?= $date->format("Y"); ?>
			</p>
			<p class="autor"><i class="fas fa-user"></i>Postado por Admin</p>							
		</li>	

<?php
		}
	}
?>	

